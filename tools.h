#include <stdio.h>
#include <fcntl.h>
#include <map>
#include <set>
#include <sstream>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <errno.h>

std::set<char> escapable{'\'', '\"', '*'};
std::map<char, std::string> replacement{
  {'\n', "\\n"},
  {'\t', "\\t"},
  {'\r', "\\r"}
};


// TODO: reliable escaping!
// consider escaping all non-alpha() and non-digit() as octal or hex
std::string escape_string_shell(std::string str)
{
  std::stringstream output1;
  for(char c : str)
  {
    if(replacement.count(c) > 0)
    {
      output1 << replacement[c];
    }
    else if(escapable.count(c) > 0)
    {
      output1 << "\\";
    }
    output1 << c;
  }
  return output1.str();
}

#define read_size1 1024
std::string markdown_call(std::string filename)
{
  std::stringstream output;
  char buf[read_size1+1];
  std::cout << escape_string_shell(filename) << std::endl;
  FILE* data = popen(std::string("markdown " + escape_string_shell(filename)).c_str(), "r");
  while(!feof(data))
  {
    int n = fread((void*)buf, 1, read_size1, data);
    buf[n+1] ='\0';
    output << buf;
  }
  pclose(data);
  return output.str();
}

void set_nonblocking(int fd)
{
  int opt;
  opt = fcntl(fd, F_GETFL);
  opt |= O_NONBLOCK;
  fcntl(fd, F_SETFL, opt);
}

std::string convert_markdown(std::string input)
{
  pid_t p;
  int status;
  int fds[2];

  if(pipe(fds) == -1)
  {
    std::cerr << "Error creating pipes" << std::endl;
    exit(1);
  }

  switch(p = fork())
  {
    case 0:
      dup2(fds[1], 1);
      dup2(fds[0], 0);
      close(fds[1]);
      close(fds[0]);
      execl("/usr/bin/markdown", "markdown", (char*) 0 );
      std::cerr << "execl() failed" << std::endl;
      exit(2);
    case -1:
      std::cerr << "Could not fork()" << std::endl;
      exit(3);
    default:
      break;
  }
  char str_cpy[input.length()+1];
  std::strncpy(str_cpy, input.c_str(), input.length());
  str_cpy[input.length()+1] = '\0';
  char * str_ptr = str_cpy;
  char * str_end = str_ptr + input.length();
  int outbuffer_step = 1024;
  int inbuffer_size = 1024;
  char buffer[inbuffer_size+1];

  set_nonblocking(fds[0]);
  std::stringstream output;
  
  bool eof = false;
  while(!eof)
  {
    if(str_ptr < str_end)
    {
      if(str_ptr+outbuffer_step > str_end)
      {
        outbuffer_step = str_end - str_ptr;
      }
      int result = write(fds[1], str_ptr, outbuffer_step);
      if(result < 0)
      {
        std::cerr << "result: " << result << std::endl;
        exit(4);
      }
      str_ptr += result;
      if(str_ptr == str_end)
      {
        close(fds[1]);
      }
    }

    int read_result = read(fds[0], buffer, inbuffer_size);
    if(read_result > 0)
    {
      buffer[read_result+1] = '\0';
      output << buffer;
    }
    else if(read_result == 0)
    {
      eof = true;
    }
    else if(errno == EWOULDBLOCK || errno == EAGAIN)
    {
    }
    else
    {
      std::cerr << "read() failed: " << errno << " - " << strerror(errno) << std::endl;
      exit(5);
    }
  }
  return output.str();
}
