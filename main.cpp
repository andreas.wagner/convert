#include <fstream>
#include <string>
#include <regex>
#include <sstream>
#include <list>

#include <iostream>

enum status
{
	START,
	PARAGRAPH,
	IN_CODE,
	IN_LIST,
	IN_HEAD
} status;

enum list_stat
{
	NUMBER,
	DOT,
	BLANK
};


std::list<list_stat> list_data{};

int main(int argc, char* argv[])
{
	if(argc < 3)
		return 1;
	status = START;
	
	std::ifstream ifile(argv[1]);
	std::ofstream ofile(argv[2]);
	
	std::regex heading1("^# (.*)", std::regex::ECMAScript | std::regex::multiline);
	std::regex heading2("^## (.*)", std::regex::ECMAScript | std::regex::multiline);
	std::regex heading3("^### (.*)", std::regex::ECMAScript | std::regex::multiline);
	std::regex code("^```", std::regex::ECMAScript | std::regex::multiline);
	std::regex link("/\\[\\[(.*)\\|(.*)\\]\\]/");
	
	ofile << "<!DOCTYPE html>\n<html>\n<head>\n";
	ofile << "</head>\n<body>\n";
	
	while(ifile.good())
	{
		std::string line;
		std::getline(ifile, line);
		bool repeat = true;
		do
		{
			std::smatch matches;
			std::list<list_stat> tmp;
			switch(status)
			{
			case START:
				if(std::regex_search(line, matches, heading1))
				{
					ofile << "<h1>" << matches[0] << "</h1>\n";
				}
				else if(std::regex_search(line, matches, heading2))
				{
					ofile << "<h2>" << matches[0] << "</h2>\n";
				}
				else if(std::regex_search(line, matches, heading3))
				{
					ofile << "<h3>" << matches[0] << "</h3>\n";
				}
				else if(std::regex_search(line, matches, code)) // TODO: consider adding support for syntax-heighlighting
				{
					ofile << "<code><pre>\n"; 
					status = IN_CODE;
				}
				else if(line == "---")
				{
					status = IN_HEAD;
				}
				else if(line.size() > 2 && (line.substr(0,3) == " # "))
				{
					status = IN_LIST;
					repeat = true;
				}
				else
				{
					ofile << "<p>\n" << line << "\n";
				}
				repeat = false;
				break;
			case PARAGRAPH:
				if(std::regex_search(line, matches, heading1)
					|| std::regex_search(line, matches, heading2)
					|| std::regex_search(line, matches, heading3)
					|| std::regex_search(line, matches, code)
					|| line.size() == 0
					|| (line.size() > 2 && line.substr(0,3) == " # "))
				{
					ofile << "</p>\n"; 
					status = START;
					repeat = true;
				}
				else
				{
					ofile << line << "\n";
				}
				break;
			case IN_CODE:
				if(std::regex_search(line, matches, code))
				{
					ofile << "</pre></code>";
					status = START;
				}
				else
				{
					ofile << line << "\n";
				}
				repeat = false;
				break;
			case IN_LIST:
				for(int i = 0;
					line[i] == ' '
					&& (line[i+1] == '#'
						|| line[i+1] == ' '
						|| line[i+1] == '-'
						|| line[i+1] == '+'
						|| line[i+1] == '*')
					&& line[i+2] == ' ';
				i+=2)
				{
					switch(line[i+1])
					{
					case '#':
						tmp.push_back(NUMBER);
						break;
					case '-':
					case '+':
					case '*':
						tmp.push_back(DOT);
						break;
					case ' ':
						tmp.push_back(BLANK);
						break;
					default:
						exit(1);
					}
				}
				for(int i = 0; i < tmp.size() || i < //TODO)
				repeat = false;
				break;
			case IN_HEAD:
				if(line == "---")
					status = START;
				repeat = false;
				break;
			default:
				exit(1);
			}
		}
		while(repeat);
	}
	ofile << "</body>\n</html>\n"; 
	return 0;
}
