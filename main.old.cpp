#include <sstream>
#include <iostream>
#include <fstream>
#include <set>
#include <filesystem>

#include "tools.h"

std::set<std::string> md_endings{"md", "markdown"};

int main(void)
{
  std::filesystem::path working_dir(".");
  std::filesystem::create_directory("html");
  for(auto name : std::filesystem::directory_iterator(working_dir))
  {
    std::stringstream target_name;
    target_name << "./html/";
    target_name << name.path().filename().string();
    std::filesystem::path target(target_name.str());
    std::stringstream output;
    int pos = name.path().filename().string().rfind('.');
    std::cout << name.path().filename().string() << ": " << pos << std::endl;
    if(pos != std::string::npos)
    {
      std::cout << name.path().filename().string().substr(pos+1) << std::endl;
      if(md_endings.count(name.path().filename().string().substr(pos+1)) > 0)
      {
        std::cout << "converting" << std::endl;
        output << "<!DOCTYPE html>\n<html><head></head><body>\n";
//        output << markdown_call(name.path().string());
        std::fstream input_file(name.path(), std::ios::in);
        std::stringstream input_stream;
        while(input_file.good())
        {
          std::string line;
          std::getline(input_file, line);
          input_stream << line << "\n";
        }
        output << convert_markdown(input_stream.str());
        output << "\n</body></html>";
        std::fstream html_file(std::string("./html/")+(name.path().filename().string().substr(0, pos) + ".html"), std::ios::out | std::ios::trunc);
        html_file << output.str();
        html_file.close();
      }
      else if(name.path().string().find("/html") != std::string::npos)
      {
        std::cout << "copying" << std::endl;
        std::filesystem::copy(name.path(), target);
      }
    }
  }
  return 0;
}
